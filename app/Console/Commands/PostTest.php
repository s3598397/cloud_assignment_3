<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class PostTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = 'https://3gk0emol7i.execute-api.us-east-1.amazonaws.com/prod/sendInvitation';
        $data = [
            "RecipientEmailAddress" => "s3598397@student.rmit.edu.au",
            "yourName"              => "Aaron",
            "theirName"             => "Bob",
            "friendlyName"          => "Ptickup",
            "make"                  => "Ford",
            "model"                 => "ranger",
            "year"                  => 2014,
            "link"                  => route('vehicle.share.accept', 123)
        ];

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
        "Accept: application/json",
        "Content-Type: text/plain",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $data = [
            "RecipientEmailAddress" => "s3598397@student.rmit.edu.au",
            "yourName"              => "Aaron",
            "theirName"             => "Bob",
            "friendlyName"          => "Ptickup",
            "make"                  => "Ford",
            "model"                 => "ranger",
            "year"                  => 2014,
            "link"                  => route('vehicle.share.accept', 123)
        ];

        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));

        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);
        var_dump($resp);




        // // use key 'http' even if you send the request to https://...
        // $options = array(
        //     'http' => array(
        //         'header'  => "Content-type: text/plain\r\n",
        //         'method'  => 'POST',
        //         'content' => ($data)
        //     )
        // );
        // $context  = stream_context_create($options);
        // $result = file_get_contents($url, false, $context);
        // if ($result === FALSE) { /* Handle error */ }

        // var_dump($result);

        //var_dump($this->httpPost($url, $data));
    }

    private function httpPost($url, $data)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }
}
