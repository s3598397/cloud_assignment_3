<?php

namespace App\Console\Commands;

use Aws\Sdk;
use Aws\Lambda\LambdaClient;
use Illuminate\Console\Command;
use Aws\Credentials\Credentials;

class LambdaTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lamdba:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $sdk = new Sdk([
            'region'   => 'us-east-1',
            'version'  => 'latest'
        ]);

        $client = $sdk->createLambda();

        $json = [
            "RecipientEmailAddress" => "s3598397@student.rmit.edu.au",
            "yourName"              => "Aaron",
            "theirName"             => "Bob",
            "friendlyName"          => "Pickup Truck",
            "make"                  => "Ford",
            "model"                 => "Ranger",
            "year"                  => "2014",
            "link"                  => "https://www.google.com.au"
        ];

        $result = $client->invoke(array(
            // FunctionName is required
            'FunctionName' => 'sendInvitation',
            //'InvocationType' => 'string',
            //'LogType' => 'string',
            //'ClientContext' => 'string',
            'Payload' => json_encode($json),
            //'Qualifier' => 'string',
        ));

        var_dump($result);

        
        // $credentials = new Credentials('YOUR_ACCESS_KEY', 'YOUR_SECRET_KEY');
        // $client = LambdaClient::factory(array(
        //     'credentials' => $credentials,
        //     'region' => 'us-east-1',
        //     'version' => 'latest'
        // ));
        // $result = $client->getFunction(array(
        //     'FunctionName' => 'expo2020-metrics',
        // ));
    }
}
