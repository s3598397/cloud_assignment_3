<?php

namespace App\Console\Commands;

use Aws\Sdk;
use Aws\DynamoDb\Marshaler;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Aws\DynamoDb\Exception\DynamoDbException;

class UploadPetrolStations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload:petrol-stations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Petrol Station dataset to dynomoDB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $counter = 0;
        $file = fopen(storage_path('app\PetrolStations_v1.csv'), "r");
        
        while ( ($data = fgetcsv($file)) !==FALSE ) {
            if ($counter == 0 ) {
                $counter++;
                continue; // skip headers
            }
            
            $sdk = new Sdk([
                'region'   => 'us-east-1',
                'version'  => 'latest'
            ]);
            
            $dynamodb = $sdk->createDynamoDb();
            $marshaler = new Marshaler();
            
            $tableName = 'fuel_stations';

            $json = json_encode([
                'object_id' => $data[0],
                'name' => $data[5],
                'operational' => $data[6],
                'owner' => $data[7],
                'address' => $data[9],
                'suburb' => $data[10],
                'state' => $data[11],
                'latitude' => $data[15],
                'longitude' => $data[16],
            ]);
        
            $params = [
                'TableName' => $tableName,
                'Item' => $marshaler->marshalJson($json)
            ];
        
            try {
                $result = $dynamodb->putItem($params);
            } catch (DynamoDbException $e) {
                echo $e->getMessage() . "\n";
                break;
            }

            $counter++;
        }
        
    }

}
