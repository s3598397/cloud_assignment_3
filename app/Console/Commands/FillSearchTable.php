<?php

namespace App\Console\Commands;

use Aws\Sdk;
use App\Models\Vehicle;
use Aws\DynamoDb\Marshaler;
use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Aws\DynamoDb\Exception\DynamoDbException;

class FillSearchTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vehicle:search';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $vehicles = Vehicle::all();

        foreach($vehicles as $vehicle) {
            $sdk = new Sdk([
                'region'   => 'us-east-1',
                'version'  => 'latest'
            ]);
            
            $dynamodb = $sdk->createDynamoDb();
            $marshaler = new Marshaler();
            
            $tableName = 'vehicles';

            $json = json_encode([
                'name' => $vehicle->friendly_name,
                'make' => $vehicle->make,
                'model' => $vehicle->model,
                'year' => $vehicle->year,
                'user_id' => $vehicle->user_id,
                'vehicle_id' => $vehicle->id
            ]);
        
            $params = [
                'TableName' => $tableName,
                'Item' => $marshaler->marshalJson($json)
            ];
        
            try {
                $result = $dynamodb->putItem($params);
            } catch (DynamoDbException $e) {
                echo $e->getMessage() . "\n";
                break;
            }
        }
    }
}
