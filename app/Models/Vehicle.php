<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Vehicle extends Model
{
    use HasFactory;

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }

    public function vehicleRecordType() 
    {
        return $this->hasOne(VehicleRecordType::class);
    }

    public function getImageURL($fileName) {
        $url = Storage::disk('s3')->temporaryUrl(
            "vehicles/{$fileName}", now()->addMinutes(5)
        );
        return $url;
    }
}
