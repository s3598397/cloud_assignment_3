<?php

namespace App\Models;

use App\Models\VehicleRecord;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class VehicleRecordType extends Model
{
    use HasFactory;

    protected $table = 'vehicle_record_types';

    public function vehicleRecord() 
    {
        return $this->belongsToMany(VehicleRecord::class);
    }
}
