<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleShared extends Model
{
    protected $table = 'vehicle_shared';

    use HasFactory;
}
