<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use App\Models\VehicleShared;
use Aws\Signature\SignatureV4;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Aws\Credentials\CredentialProvider;
use GuzzleHttp\Psr7\Request as AwsRequest;

class SearchController extends Controller
{

    public function search(Request $request) {

        $search = $request->search;

        $vehicles = Vehicle::whereIn('vehicles.id', function($query) use ($search) {
                            $query->select('vehicle_id')
                                    ->from(with(new VehicleShared())->getTable())
                                    ->where('accepted', 1)
                                    ->where('user_id', Auth::user()->id)
                                    ->where('friendly_name', 'like', '%' . $search . '%');
                        })
                        ->orWhereIn('vehicles.id', function($query) use ($search) {
                            $query->select('id')
                                    ->from(with(new Vehicle())->getTable())
                                    ->where('user_id', Auth::user()->id)
                                    ->where('friendly_name', 'like', '%' . $search . '%');
                        })
                        ->join('users', 'users.id', 'vehicles.user_id')
                        ->select('vehicles.*', 'users.given_name', 'users.family_name')
                        ->get();

        foreach($vehicles as $vehicle) {
            $vehicle->imageURL = $vehicle->getImageURL($vehicle->fileName);
        }

        $searched = true;
        return view('vehicle.index', compact('vehicles', 'searched'));
    }

    //
    // public function searchADomain(Request $request)
    // {
    //     $domainName = 's3598397a3v2';
    //     $domainId = 'm2vrmqq5tszqgrlxql4ai4wvdu';
    //     $domainRegion = 'us-east-1';
    //     // $searchString = "q=(and name:'{$request->search}'+(phrase+field='user_id'+'2'))&q.parser=structured";
    //     $searchString = "q={$request->search}";
    //     $client = new Client();

    //     $vehiclesSearched = $this->searchDomain($client, $domainName, $domainId, $domainRegion, $searchString);

    //     $vehicles = Vehicle::whereIn('vehicles.id', function($query) use ($vehiclesSearched) {
    //                             $query->select('vehicle_id')
    //                                     ->from(with(new VehicleShared())->getTable())
    //                                     ->where('accepted', 1)
    //                                     ->where('user_id', Auth::user()->id)
    //                                     ->whereIn('vehicles.id', $vehiclesSearched);
    //                         })
    //                         ->orWhereIn('vehicles.id', function($query) use ($vehiclesSearched) {
    //                             $query->select('id')
    //                                     ->from(with(new Vehicle())->getTable())
    //                                     ->where('user_id', Auth::user()->id)
    //                                     ->whereIn('vehicles.id', $vehiclesSearched);
    //                         })
    //                         ->join('users', 'users.id', 'vehicles.user_id')
    //                         ->select('vehicles.*', 'users.given_name', 'users.family_name')
    //                         ->get();

    //     //User::findOrFail(Auth::user()->id)->vehicles()->get();

    //     foreach($vehicles as $vehicle) {
    //         $vehicle->imageURL = $vehicle->getImageURL($vehicle->fileName);
    //     }

    //     $searched = true;
    //     return view('vehicle.index', compact('vehicles', 'searched'));
    // }

    // private function searchDomain($client, $domainName, $domainId, $domainRegion, $searchString) {
    //     $domainPrefix = 'search-';
    //     $cloudSearchDomain = 'cloudsearch.amazonaws.com';
    //     $cloudSearchVersion = '2013-01-01';
    //     $searchPrefix = 'search?';

    //     // Specify the search to send.
    //     $request = new AwsRequest(
    //         'GET',
    //         "https://{$domainPrefix}{$domainName}-{$domainId}.{$domainRegion}." .
    //             "{$cloudSearchDomain}/{$cloudSearchVersion}/" .
    //             "{$searchPrefix}{$searchString}"
    //     );

    //     // Get default AWS account access credentials.
    //     $credentials = call_user_func(CredentialProvider::defaultProvider())->wait();

    //     // Sign the search request with the credentials.
    //     $signer = new SignatureV4('cloudsearch', $domainRegion);
    //     $request = $signer->signRequest($request, $credentials);

    //     // Send the signed search request.
    //     $response = $client->send($request);

    //     // Report the search results, if any.
    //     $results = json_decode($response->getBody());

    //     // dd($results);
    //     $vehicles = array();

    //     if ($results->hits->found > 0) {
    //         foreach($results->hits->hit as $hit) {
    //             $vehicles[] = $hit->fields->vehicle_id;
    //         }
    //     }
        
    //     return $vehicles;
    // }

    
}
