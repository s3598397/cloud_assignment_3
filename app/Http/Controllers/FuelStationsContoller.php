<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use App\Models\VehicleShared;
use Aws\Signature\SignatureV4;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Aws\Credentials\CredentialProvider;
use GuzzleHttp\Psr7\Request as AwsRequest;

class FuelStationsContoller extends Controller
{
    public function index() {
        return view('stations.index');
    }

    public function search(Request $request)
    {
        $domainName = getenv('AWS_CLOUD_SEARCH_DOMAIN');
        $domainId = getenv('AWS_CLOUD_SEARCH_DOMAIN_ID');
        $domainRegion = getenv('AWS_CLOUD_SEARCH_REGION');
        
        // $searchString = "q=(and name:'{$request->search}'+(phrase+field='user_id'+'2'))&q.parser=structured";
        $searchString = "q={$request->search}";
        $client = new Client();

        $stations = $this->searchDomain($client, $domainName, $domainId, $domainRegion, $searchString);

        return view('stations.results', compact('stations'));
    }

    private function searchDomain($client, $domainName, $domainId, $domainRegion, $searchString) {
        $domainPrefix = 'search-';
        $cloudSearchDomain = 'cloudsearch.amazonaws.com';
        $cloudSearchVersion = '2013-01-01';
        $searchPrefix = 'search?';

        // Specify the search to send.
        $request = new AwsRequest(
            'GET',
            "https://{$domainPrefix}{$domainName}-{$domainId}.{$domainRegion}." .
                "{$cloudSearchDomain}/{$cloudSearchVersion}/" .
                "{$searchPrefix}{$searchString}"
        );

        // Get default AWS account access credentials.
        $credentials = call_user_func(CredentialProvider::defaultProvider())->wait();

        // Sign the search request with the credentials.
        $signer = new SignatureV4('cloudsearch', $domainRegion);
        $request = $signer->signRequest($request, $credentials);

        // Send the signed search request.
        $response = $client->send($request);

        // Report the search results, if any.
        $results = json_decode($response->getBody());

        // dd($results);
        $vehicles = array();

        if ($results->hits->found > 0) {
            foreach($results->hits->hit as $hit) {
                $vehicles[] = $hit->fields;
            }
        }
        
        return $vehicles;
    }

}
