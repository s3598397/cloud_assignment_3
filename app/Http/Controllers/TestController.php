<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Aws\Exception\AwsException;
use App\Http\Controllers\Controller;
use Aws\ApiGateway\ApiGatewayClient;

class TestController extends Controller
{
    //

    public function getBasePathMapping($apiGatewayClient, $basePath, $domainName)
    {
        // try {
            $result = $apiGatewayClient->getBasePathMapping([
                'basePath' => $basePath,
                'domainName' => $domainName,
            ]);
            return 'The base path mapping\'s effective URI is: ' . 
                $result['@metadata']['effectiveUri'];
        // } catch (AwsException $e) {
        //     return 'Error: ' . $e['message'];
        // }
    }

    public function getsTheBasePathMapping()
    {
        
        $apiGatewayClient = new ApiGatewayClient([
            'region' => 'us-east-1',
            'version' => '2015-07-09'
        ]);
        
        echo $this->getBasePathMapping($apiGatewayClient, '(none)', '00uz6403kb.execute-api.us-east-1.amazonaws.com');
    }
}
