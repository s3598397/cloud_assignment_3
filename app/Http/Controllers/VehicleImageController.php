<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Aws\Rekognition\RekognitionClient;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use League\CommonMark\Inline\Element\Image;

class VehicleImageController extends Controller
{
    //

    public function check(Request $request) {

        try {
            $client = new RekognitionClient([
                'region'    => 'us-west-2',
                'version'   => 'latest'
            ]);
        
            $image = fopen($request->file('image')->getPathName(), 'r');
            $bytes = fread($image, $request->file('image')->getSize());

            $results = $client->detectLabels(
                ['Image' => ['Bytes' => $bytes], 
                'MinConfidence' => 50]
            );

            if (isset($results['Labels'][0]['Name'])) {
                $label = $results['Labels'][0]['Name'];
            } else {
                $label = "";
            }
            
            $response = array(
                'success' => true,
                'label' => $label,
            );
        } catch (Exception $e) {
            $response = array(
                'success' => false
            );
        }
        
        return response()->json($response);
    }

    public function retrieve($vehicleId) {
        $vehicle = Vehicle::find($vehicleId)->where('user_id', Auth::user()->id)->first();
        // return base64_encode(file_get_contents($vehicle->getImageURL($vehicle->fileName)));
        return response()->file(base64_encode(file_get_contents($vehicle->getImageURL($vehicle->fileName))));
        //return $vehicle->getImageURL();
        $fileContent = $vehicle->getImageURL($vehicle->fileName);
        return Response::stream(function() use($fileContent) {
            echo $fileContent;
        }, 200, array());
    }

}
