<?php

namespace App\Http\Controllers;

use DateTime;
use App\Models\User;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use App\Models\VehicleRecord;
use App\Models\VehicleRecordType;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\VehicleShared;
use Illuminate\Support\Facades\Auth;
use App\Repositories\VehicleRepository;

class VehicleRecordController extends Controller
{
    private $pageTitle = "Vehicle Tracker";
    /**
     * Display a listing of all the records for the specified vehicles.
     *
     * @return \Illuminate\Http\Response
     */

    private function getVehicle($vehicle_id) {
        return Vehicle::whereIn('vehicles.id', function($query){
                            $query->select('vehicle_id')
                            ->from(with(new VehicleShared())->getTable())
                            ->where('accepted', 1)
                            ->where('user_id', Auth::user()->id);
                        })
                        ->orWhere('vehicles.user_id', Auth::user()->id)
                        ->where('id', $vehicle_id)->firstOrFail();
    }

    public function index(int $vehicle_id)
    {
        $pageTitle = $this->pageTitle;
        // $vehicle = User::findOrFail(Auth::user()->id)->vehicles()->where('id', $vehicle_id)->firstOrFail();
        $vehicle = Vehicle::whereIn('vehicles.id', function($query){
                        $query->select('vehicle_id')
                        ->from(with(new VehicleShared())->getTable())
                        ->where('accepted', 1)
                        ->where('user_id', Auth::user()->id);
                    })
                    ->orWhere('vehicles.user_id', Auth::user()->id)
                    ->join('users', 'users.id', 'vehicles.user_id')
                    ->select('vehicles.*', 'users.given_name', 'users.family_name')
                    ->where('vehicles.id', $vehicle_id)->firstOrFail();

        $vehicle->imageURL = $vehicle->getImageURL($vehicle->fileName);
        $subTitle = "For {$vehicle->name}";

        $vehicle_records = DB::table('vehicle_records')
                            ->select([
                                'vehicle_records.id',
                                'vehicle_record_type_id',
                                'vehicle_record_types.type',
                                'date',
                                'liters',
                                'cost',
                                'kms',
                                'fuel_cost',
                                'description',
                                'adder.given_name',
                                'adder.family_name'
                            ])
                            ->join('vehicle_record_types', 'vehicle_record_type_id', '=', 'vehicle_record_types.id')
                            ->join('users as adder', 'adder.id', 'vehicle_records.added_by_id')
                            ->where('active', 1)
                            ->where('vehicle_id', $vehicle_id)
                            ->orderBy('date')
                            ->paginate(30, ['*'], 'vehicle_records');

        
        $fuel_summaries = DB::table('vehicles')
                            ->join('vehicle_records', 'vehicles.id' , 'vehicle_records.vehicle_id')
                            ->where('vehicles.id', $vehicle_id)
                            ->where('vehicle_record_type_id', 1)
                            ->select(
                                    DB::raw("SUM(kms) as kms"),
                                    DB::raw("SUM(cost) as cost"),
                                    DB::raw("SUM(liters) as liters"))
                            ->first();

        $totalCost = DB::table('vehicles')
                            ->join('vehicle_records', 'vehicles.id' , 'vehicle_records.vehicle_id')
                            ->where('vehicles.id', $vehicle_id)
                            ->select(DB::raw("SUM(cost) as cost"))
                            ->first();

        $totalUsers = VehicleShared::where('vehicle_id', $vehicle_id)->count('id');
                            
        return view('vehicle.record-index', compact('vehicle', 'vehicle_records', 'fuel_summaries', 'totalUsers', 'totalCost', 'pageTitle', 'subTitle'));
    }

    /**
     * Show the form for creating a new vehicle record.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(int $vehicle_id)
    {
        $pageTitle = $this->pageTitle;
        // $vehicle = Vehicle::where('id', $vehicle_id)->where('user_id', Auth::user()->id)->firstOrFail();
        $vehicle = $this->getVehicle($vehicle_id);
        $subTitle = "For {$vehicle->name}";

        $vehicle_record_types = VehicleRecordType::get();

        return view('vehicle.record-edit', compact('vehicle', 'vehicle_record_types', 'pageTitle', 'subTitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($vehicle_id, Request $request)
    {
        $request->validate([
            'vehicle_record_type_id' => ['required', 'exists:vehicle_record_types,id', 'integer'],
            'date' => ['required', 'date_format:d/m/Y'],
            'liters' => ['nullable', 'numeric'],
            'cost' => ['required', 'numeric'],
            'kms' => ['nullable', 'numeric'],
            'fuel_cost' => ['nullable', 'numeric'],
            'description' => ['nullable', 'string']
        ]);

        $date = DateTime::createFromFormat('d/m/Y', $request->date);
        $request->date = $date->format('Y-m-d');

        $data                           = new VehicleRecord;
        $data->vehicle_id               = $vehicle_id;
        $data->vehicle_record_type_id   = $request->vehicle_record_type_id;
        $data->date                     = date('Y-m-d', strtotime($request->date));
        $data->liters                   = $request->liters;
        $data->cost                     = $request->cost;
        $data->kms                      = $request->kms;
        $data->fuel_cost                = $request->fuel_cost;
        $data->description              = $request->description;
        $data->added_by_id              = Auth::user()->id;

        $data->save();

        return redirect()->route('vehicle.record.index', $vehicle_id)
                         ->with('success', "Vehicle Record successfully created!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\vehicleRecord  $vehicleRecord
     * @return \Illuminate\Http\Response
     */
    public function show(VehicleRecord $vehicleRecord)
    {
        //
    }

    /**
     * Show the form for editing the specified vehicle record.
     *
     * @param  \App\Models\vehicleRecord  $vehicleRecord
     * @return \Illuminate\Http\Response
     */
    public function edit(int $vehicle_record_id)
    {
        $pageTitle = $this->pageTitle;
        $vehicle = DB::table('vehicles')
                    ->join('vehicle_records', 'vehicle_records.vehicle_id', 'vehicles.id')
                    ->select('vehicles.*')
                    ->where('vehicle_records.id', $vehicle_record_id)
                    ->get();
                    // dd($vehicle);
        $vehicle = $vehicle[0];
        $vehicle = $this->getVehicle($vehicle->id);

        // $vehicle = VehicleRecord::findOrFail($vehicle_record_id)->vehicle()->where('user_id', Auth::user()->id)->firstOrFail();
        // $subTitle = "For {$vehicle->name}";

        $vehicle_record = VehicleRecord::where('id', $vehicle_record_id)->firstOrFail();

        $vehicle_record_types = VehicleRecordType::get();

        return view('vehicle.record-edit', compact('vehicle', 'vehicle_record', 'vehicle_record_types', 'pageTitle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\vehicleRecord  $vehicleRecord
     * @return \Illuminate\Http\Response
     */
    public function update($vehicle_record_id, Request $request)
    {
        $request->validate([
            'vehicle_record_type_id' => ['required', 'exists:vehicle_record_types,id', 'integer'],
            'date' => ['required', 'date_format:d/m/Y'],
            'liters' => ['nullable', 'numeric'],
            'cost' => ['required', 'numeric'],
            'kms' => ['nullable', 'numeric'],
            'fuel_cost' => ['nullable', 'numeric'],
            'description' => ['nullable', 'string']
        ]);

        //find the vehicle for the user
        $vehicle = VehicleRecord::findOrFail($vehicle_record_id)->vehicle()->where('user_id', Auth::user()->id)->firstOrFail();

        $date = DateTime::createFromFormat('d/m/Y', $request->date);
        $request->date = $date->format('Y-m-d');

        DB::table('vehicle_records')
            ->where('id', $vehicle_record_id)
            ->update([
                'vehicle_record_type_id' => $request->vehicle_record_type_id,
                'date' => date('Y-m-d', strtotime($request->date)),
                'liters' => $request->liters,
                'cost' => $request->cost == null ? 0 : $request->cost,
                'kms' => $request->kms,
                'fuel_cost' => $request->fuel_cost,
                'description' => $request->description
            ]);

        return redirect()->route('vehicle.record.index', $vehicle->id)
                        ->with('success', "Vehicle #{$vehicle_record_id} successfully updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\vehicleRecord  $vehicleRecord
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('vehicle_records')
            ->where('id', $request->id)
            ->update([
                'active' => 0
            ]);

        $response = array(
            'status' => 'success',
            'msg' => "Vehicle Record #{$request->id} successfully archived!",
        );

        return response()->json($response);
    }
}
