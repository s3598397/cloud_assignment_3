<?php

namespace App\Http\Controllers;

use Aws\Sdk;
use Exception;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\VehicleShared;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class VehicleShareController extends Controller
{
    //

    public function create(int $vehicle_id) {
        $vehicle = User::findOrFail(Auth::user()->id)->vehicles()->where('id', $vehicle_id)->firstOrFail();

        return view('vehicle.share.index', compact('vehicle'));
    }

    public function store(Request $request) {
        $request->validate([
            'email' => ['required', 'exists:users,email', 'string', 'email'],
            'vehicle_id' => Rule::exists('vehicles', 'id')->where(function ($query) use ($request) {
                return $query->where('id', $request->vehicle_id)->where('user_id', Auth::user()->id);
            })
        ]);

        //Get sharing to users details
        try {
            $userTo = User::where('email', $request->email)->firstOrFail();
        } catch (Exception $e) {
            return back()->withErrors(['msg' => "User details could not be located. Please try again."]);
        }

        try {
            $vehicle = User::findOrFail(Auth::user()->id)->vehicles()->where('id', $request->vehicle_id)->firstOrFail();
        } catch (Exception $e) {
            return back()->withErrors(['msg' => "Vehicle details could not be located. Please try again."]);
        }

        //Delete any pending requests
        $delete = VehicleShared::where('vehicle_id', $request->vehicle_id)->where('user_id', $userTo->id)->where('accepted', 0)->delete();

        //Reject if already accepted
        $accepts = VehicleShared::where('vehicle_id', $request->vehicle_id)->where('user_id', $userTo->id)->where('accepted', 1)->get();
        if (count($accepts) > 0) {
            return back()->withErrors(['msg' => "This vehicle has already been shared to this user."]);
        }

        try {
            $token = sha1(time());

            $share = new VehicleShared;
            $share->vehicle_id = $request->vehicle_id;
            $share->user_id = $userTo->id;
            $share->accepted = 0;
            $share->token = $token;
            $share->save();

            $vehicleId = $vehicle->id;
        } catch (Exception $e) {
            return back()->withErrors(['msg' => "An error occured while creating the invitation. Please try again"]);
        }
        
        try {
            // $sdk = new Sdk([
            //     'region'   => 'us-east-1',
            //     'version'  => 'latest'
            // ]);

            // $client = $sdk->createLambda();

            $json = [
                "RecipientEmailAddress" => Auth::user()->email,
                "yourName"              => $userTo->given_nam,
                "theirName"             => Auth::user()->given_name,
                "friendlyName"          => $vehicle->friendly_name,
                "make"                  => $vehicle->make,
                "model"                 => $vehicle->model,
                "year"                  => $vehicle->year,
                "link"                  => route('vehicle.share.accept', $token)
            ];

            $url = getenv('AWS_SEND_INVITATION');

            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            $headers = array(
                "Accept: application/json",
                "Content-Type: text/plain",
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($json));

            //for debug only!
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            $resp = curl_exec($curl);
            curl_close($curl);
            var_dump($resp);

            // if ($resp['message'] == "Internal Server Error") {
            //     throw new Exception("Unable to send invitation email via API gateway");
            // }

            // $result = $client->invoke(array(
            //     'FunctionName' => 'sendInvitation',
            //     'Payload' => json_encode($json),
            // ));
        } catch (Exception $e) {
            return back()->withErrors(['msg' => "An error occured while sending the email invitation. Please try again"]);
        }

        return redirect()->route('vehicle.record.index', $request->vehicle_id)->with('success', "Successfully invited {$request->email}. They will recieve an invitation email within the next 10 minutes.");
    }

    public function accept($token) {
        $vehicleShared = VehicleShared::where('token', $token)->first();
        if (isset($vehicleShared)){
            if (Auth::user()->id != $vehicleShared->user_id) {
                return redirect()->route('vehicle.index')->withErrors(["This invitation is not for you. Please login to the invited users account to accept the invitation."]);
            } else {
                if (!$vehicleShared->accepted) {
                    $vehicleShared->accepted = 1;
                    $vehicleShared->save();
                    $status = "You have accepted the invitation. You can now interact with this vehicle.";
                } else {
                    $status = "You have already accepted the invitation.";
                }
            }            
        } else {
            return redirect()->route('vehicle.index')->withErrors(["Sorry your invitation cannot be identified."]);
        }

        return redirect()->route('vehicle.index')->with('success', $status);
    }
}
