<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use BlackBits\LaravelCognitoAuth\CognitoClient;

class VerificationCodeController extends Controller
{
    //

    public function index($username) {
        return view('auth.verification-code', compact('username'));
    }

    public function verify($username, Request $request) {
        $validated = $request->validate([
            'code' => 'required'
        ]);

        $response = app()->make(CognitoClient::class)->confirmUserSignUp($username, $request->code);

        if ($response == 'validation.invalid_user') {
            return redirect()->back()
                ->withErrors(['code' => trans('black-bits/laravel-cognito-auth::validation.invalid_user')]);
        }

        if ($response == 'validation.invalid_token') {
            return redirect()->back()
                ->withErrors(['code' => trans('black-bits/laravel-cognito-auth::validation.invalid_token')]);
        }

        if ($response == 'validation.exceeded') {
            return redirect()->back()
                ->withErrors(['code' => trans('black-bits/laravel-cognito-auth::validation.exceeded')]);
        }

        if ($response == 'validation.confirmed') {
            return redirect('/home')->with('verified', true);
        }

        return redirect('/home')->with('verified', true);
    }

    public function resend($username) {
        app()->make(CognitoClient::class)->resendToken($username);

        return back()->with('resent', true);
    }

}
