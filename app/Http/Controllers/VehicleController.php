<?php

namespace App\Http\Controllers;

use Aws\Sdk;
use Exception;
use App\Models\User;
use App\Models\Vehicle;
use Aws\DynamoDb\Marshaler;
use Illuminate\Http\Request;
use App\Models\VehicleRecord;
use App\Models\VehicleShared;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Repositories\VehicleRepository;
use Illuminate\Support\Facades\Storage;
use BiegalskiLLC\NHTSAVehicleAPI\VehicleApi;
use Aws\DynamoDb\Exception\DynamoDbException;

class VehicleController extends Controller
{
    private $pageTitle = "Vehicle Tracker";

    /**
     * Display a listing of the users vehicles.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = $this->pageTitle;
        $subTitle = "";

        $vehicles = Vehicle::whereIn('vehicles.id', function($query){
                        $query->select('vehicle_id')
                        ->from(with(new VehicleShared())->getTable())
                        ->where('accepted', 1)
                        ->where('user_id', Auth::user()->id);
                    })
                    ->orWhere('vehicles.user_id', Auth::user()->id)
                    ->join('users', 'users.id', 'vehicles.user_id')
                    ->select('vehicles.*', 'users.given_name', 'users.family_name')
                    ->get();
        
        //User::findOrFail(Auth::user()->id)->vehicles()->get();

        foreach($vehicles as $vehicle) {
            $vehicle->imageURL = $vehicle->getImageURL($vehicle->fileName);
        }
        
        return view('vehicle.index', compact('vehicles', 'pageTitle'));
    }

    /**
     * Show the form for creating a new vehicle.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageTitle = $this->pageTitle;
        $subTitle = "Create new vehicle";

        $vehicles   = new VehicleApi();
        $cars       = $vehicles->listPreloadedMakes('car/truck');
        $bikes      = $vehicles->listPreloadedMakes('motorcycle/scooter');
        $years      = $vehicles->listYears();
        // dd($years);
        // return view('vehicle.create', compact('cars', 'bikes'));

        return view('vehicle.edit', compact('pageTitle', 'cars', 'bikes', 'years'));
    }

    /**
     * Get models of cars based off make.
     *
     * @return \Illuminate\Http\Response
     */
    public function getModels(Request $request) {
        $request->validate([
            'make' => ['required', 'string'],
            'year' => ['required', 'string']
        ]);

        try {
            $vehicles   = new VehicleApi();
            $models = $vehicles->listModelsByMakeYear($request->make, $request->year);
    
            $response = array(
                'success' => true,
                'models' => $models,
            );
        } catch (Exception $e) {
            $response = array(
                'success' => false
            );
        }
        
        return response()->json($response);

    }

    /**
     * Store a newly created vehicle in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'make' => ['required', 'string', 'max:225'],
            'year' => ['required', 'string', 'max:225'],
            'model' => ['required', 'string', 'max:225'],
            'friendly_name' => ['required', 'string', 'max:225'],
            'image' => ['required', 'mimes:jpeg,png']
        ]);

        $vehicle = new Vehicle;
        $vehicle->user_id = Auth::user()->id;
        $vehicle->friendly_name = $request->friendly_name;
        $vehicle->make = $request->make;
        $vehicle->year = $request->year;
        $vehicle->model = $request->model;
        $vehicle->save();

        $vehicleId = $vehicle->id;
        $fileName = "{$vehicleId}.{$request->file('image')->getClientOriginalExtension()}";

        $store = Storage::disk('s3')->put("vehicles/{$fileName}", file_get_contents($request->file('image')->getPathName()));

        $vehicle = Vehicle::find($vehicleId);
        $vehicle->fileName = $fileName;
        $vehicle->save();

        // $this->shadowCopyToDynomodb($vehicle);

        return redirect()->route('vehicle.index')
                         ->with('success', "Vehicle successfully created!");
    }

    /**
     * Display a vehicle resource.
     *
     * @param  \App\Models\vehicleRecord  $vehicleRecord
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicle $vehicle)
    {
        //
    }

    /**
     * Show the form for editing the specified vehicle.
     *
     * @param  \App\Models\vehicleRecord  $vehicleRecord
     * @return \Illuminate\Http\Response
     */
    public function edit(int $vehicle_id)
    {
        $pageTitle = $this->pageTitle;
        $subTitle = "";

        $vehicle = Vehicle::findOrFail($vehicle_id)
                            ->whereIn('vehicles.id', function($query){
                                $query->select('vehicle_id')
                                ->from(with(new VehicleShared())->getTable())
                                ->where('accepted', 1)
                                ->where('user_id', Auth::user()->id);
                            })
                            ->orWhere('vehicles.user_id', Auth::user()->id)
                            ->get();
                            
        if (!isset($vehicle[0])) {
            return back()->withErrors('Vehicle could not be located.');
        } else {
            $vehicle = $vehicle[0];
            $subTitle = "Edit {$vehicle->name}";
            return view('vehicle.edit', compact('vehicle', 'pageTitle'));
        }

    }

    /**
     * Update the specified vehicle in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\vehicleRecord  $vehicleRecord
     * @return \Illuminate\Http\Response
     */
    public function update(int $vehicle_id, Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:225']
        ]);

        $vehicle = Vehicle::findOrFail($vehicle_id)->where(Auth::user()->id);
        $vehicle->name = $request->name;
        $vehicle->save();

        return redirect()->route('vehicle.index')
                        ->with('success', "Vehicle #{$vehicle_id} successfully updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\vehicleRecord  $vehicleRecord
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $vehicle = Vehicle::findOrFail($request->vehicle_id)->where(Auth::user()->id);

        $response = array(
            'status' => 'success',
            'msg' => "Vehicle #{$request->vehicle_id} successfully removed!",
        );

        return response()->json($response);
    }

    private function shadowCopyToDynomodb($vehicle) {
        $sdk = new Sdk([
            'region'   => 'us-east-1',
            'version'  => 'latest'
        ]);
        
        $dynamodb = $sdk->createDynamoDb();
        $marshaler = new Marshaler();
        
        $tableName = 'vehicles';

        $json = json_encode([
            'name' => $vehicle->friendly_name,
            'make' => $vehicle->make,
            'model' => $vehicle->model,
            'year' => $vehicle->year,
            'user_id' => $vehicle->user_id,
            'vehicle_id' => $vehicle->id
        ]);
    
        $params = [
            'TableName' => $tableName,
            'Item' => $marshaler->marshalJson($json)
        ];
    
        try {
            $result = $dynamodb->putItem($params);
        } catch (DynamoDbException $e) {
            echo $e->getMessage() . "\n";
        }
    }
}
