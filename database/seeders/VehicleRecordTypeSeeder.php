<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Database\Seeders\Str;
use Carbon\Carbon;

class VehicleRecordTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data[] = array('type' => 'Fuel');
        $data[] = array('type' => 'Tyres');
        $data[] = array('type' => 'Service');
        $data[] = array('type' => 'Repairs');
        $data[] = array('type' => 'Insurance');
        $data[] = array('type' => 'Modifications');
        $data[] = array('type' => 'Other');

        foreach($data as $db) {
            DB::table('vehicle_record_types')->insert([
                'type' => $db['type'],
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }

    }
}
