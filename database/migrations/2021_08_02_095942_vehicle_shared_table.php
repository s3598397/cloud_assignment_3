<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VehicleSharedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('vehicle_shared')) {
            Schema::create(
                'vehicle_shared',
                static function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('vehicle_id', false, true);
                    $table->integer('user_id', false, true);
                    $table->integer('accepted')->default(0);
                    $table->string('token')->unique()->nullable();

                    //References
                    $table->foreign('vehicle_id')->references('id')->on('vehicles')->onDelete('cascade');
                    $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

                    //Table Columns
                    $table->timestamps();
                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_shared');
    }
}
