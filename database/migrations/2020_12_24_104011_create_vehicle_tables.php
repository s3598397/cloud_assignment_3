<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('vehicles')) {
            Schema::create('vehicles', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id', false, true);
                $table->string('friendly_name');
                $table->string('make');
                $table->string('model');
                $table->string('year');
                $table->string('fileName');

                //References
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('vehicle_record_types')) {
            Schema::create(
                'vehicle_record_types',
                static function (Blueprint $table) {
                    $table->increments('id');
                    $table->string('type');

                    //Table Columns
                    $table->timestamps();
                }
            );
        }

        if (!Schema::hasTable('vehicle_records')) {
            Schema::create(
                'vehicle_records',
                static function (Blueprint $table) {
                    $table->increments('id');

                    $table->integer('vehicle_id', false, true);
                    $table->integer('vehicle_record_type_id', false, true);
                    $table->date('date');
                    $table->float('liters', 8, 2)->nullable();
                    $table->float('cost', 8, 2);
                    $table->float('kms', 8, 2)->nullable();
                    $table->float('fuel_cost', 8, 2)->nullable();
                    $table->text('description')->nullable();
                    $table->integer('added_by_id', false, true);

                    //References
                    $table->foreign('vehicle_id')->references('id')->on('vehicles');
                    $table->foreign('vehicle_record_type_id')->references('id')->on('vehicle_record_types');
                    $table->foreign('added_by_id')->references('id')->on('users')->onDelete('cascade');

                    //Table Columns
                    $table->integer('active')->default(1);
                    $table->timestamps();
                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_records');
        Schema::dropIfExists('vehicle_record_types');
        Schema::dropIfExists('vehicles');
    }

}
