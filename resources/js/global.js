
let CSRF_TOKEN = "";
window.addEventListener('DOMContentLoaded', (event) => {
    CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
});


$(function() {
    $(".datepicker").datepicker(
        {
            "setDate": new Date(),
            dateFormat: 'dd/mm/yy',
            beforeShow: function(i) {
                if ($(i).attr('readonly')) { return false; }

                setTimeout(function(){
                    $('.ui-datepicker').css('z-index', 99999999999999);
                }, 0);

                $(this).attr("autocomplete", "off");
            }
        }
    );
    $('.datepicker').on('click', function(e) {
        e.preventDefault();
        $(this).attr("autocomplete", "off");
     });
     $(".datepicker").datepicker("setDate", new Date());
});
