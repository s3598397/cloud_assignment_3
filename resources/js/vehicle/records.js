
window.addEventListener('DOMContentLoaded', (event) => {
    for (btn of document.querySelectorAll(".archive-vehicle-btn")) {
        archive(btn, archiveVehicleURL, "record");
    }
});


function archive(btn, url, type) {

    btn.addEventListener('click', function(e) {
        name = e.target.dataset.name ? e.target.dataset.name : "";
        swal({
            title: `Are you sure you want to archive ${type} ${name}?`,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        id: e.target.dataset.id
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        swal({
                            title: data.msg,
                            showCloseButton: false,
                            showCancelButton: false,
                            icon: "success",
                            showConfirmButton: false,
                        }).then(function () {
                            window.location.reload();
                        });
                    }
                });
            } else {
                swal(`${type} not archived!`);
            }
        });
    });

}