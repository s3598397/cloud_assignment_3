window.addEventListener('DOMContentLoaded', (event) => {
    form = document.querySelector('form[name="vehicle"]');
    form.querySelector('select[name="vehicle_record_type_id"]').addEventListener('change', showOptions);    

    showOptions();
});

function showOptions() {
    form = document.querySelector('form[name="vehicle"]');
    vehicle_record_type_id = form.querySelector('select[name="vehicle_record_type_id"]').value

    if (vehicle_record_type_id == 1) {
        form.querySelector('input[name="liters"]').classList.remove('d-none')
        form.querySelector('label[for="liters"]').classList.remove('d-none')

        form.querySelector('input[name="kms"]').classList.remove('d-none')
        form.querySelector('label[for="kms"]').classList.remove('d-none')

        form.querySelector('input[name="fuel_cost"]').classList.remove('d-none')
        form.querySelector('label[for="fuel_cost"]').classList.remove('d-none')
    } else {
        form.querySelector('input[name="liters"]').classList.add('d-none')
        form.querySelector('label[for="liters"]').classList.add('d-none')

        form.querySelector('input[name="kms"]').classList.add('d-none')
        form.querySelector('label[for="kms"]').classList.add('d-none')

        form.querySelector('input[name="fuel_cost"]').classList.add('d-none')
        form.querySelector('label[for="fuel_cost"]').classList.add('d-none')
    }
}