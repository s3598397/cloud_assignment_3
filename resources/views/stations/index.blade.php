@extends('layouts.app')

@section('content')
    <section class="content station-content">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 col-md-10 offset-md-1 col-sm-12">
                <div class="card">
                    <div class="card-header card-header-info">
                        <h4 class="card-title">
                            Find Fuel Stations
                        </h4>
                    </div>
                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                The following error(s) occured:
                                <ul class="m-0">
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form method="POST" name="stations" action={{route('stations.results')}}>
                            @csrf
                            <div class="row">
                                <label class="col-sm-2 col-form-label" for="input-current-password">Search</label>
                                <div class="col-sm-6">
                                    <div class="form-group bmd-form-group">
                                        <input class="form-control" type="text" name="search" placeholder="Enter search term" value="{{ old('search') }}" required>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-success">Search Stations</button>
                                </div>
                            </div>
                        </form>
                    </div> 
                </div>
            </div>
        </div>
    </section>
@endsection
