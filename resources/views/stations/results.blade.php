@extends('layouts.app')

@section('content')
    {{-- <link href="https://api.mapbox.com/mapbox-gl-js/v1.8.1/mapbox-gl.css" rel="stylesheet" />
    <script src="https://api.mapbox.com/mapbox-gl-js/v1.8.1/mapbox-gl.js"></script>
    <script>
    mapboxgl.accessToken = "pk.eyJ1IjoiczM1OTgzOTciLCJhIjoiY2tzMzRwbHl2MDdxZzJ4cDliN2VjOGFhZyJ9.TLQgBspQSzqbT-bg3MQQwg";
    </script> --}}

    <section class="content station-content">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 col-md-10 offset-md-1 col-sm-12">
                <div class="card">
                    <div class="card-header card-header-info">
                        <h4 class="card-title">
                            Find Fuel Stations
                        </h4>
                    </div>
                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                The following error(s) occured:
                                <ul class="m-0">
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <p>{{ count($stations) }} stations found. <a href="{{ route('stations.search') }}" class="btn btn-info"><i class="fa fa-plus fa-fw push-right"></i> Return to search</a></p>
                        @foreach ($stations as $key => $station)
                            
                            <div class="row">
                                <div class="col-sm-8 offset-2 search-result">
                                    <div class="search-padding">
                                        <h4>{{$key + 1}}. {{ $station->name }}</h4>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <h6>Address:</h6>
                                                <p>
                                                    {{ $station->address }}<br>
                                                    {{ $station->suburb }}<br>
                                                    {{ $station->state }}
                                                </p>
                                            </div>
                                            <div class="col-sm-4">
                                                <h6>Owner:</h6>
                                                <p>
                                                    {{ $station->owner }}
                                                </p>
                                            </div>
                                            <div class="col-sm-4">
                                                <h6>Location:</h6>
                                                <p>
                                                    {{ $station->longitude }} <br>
                                                    {{ $station->latitude }}
                                                </p>
                                                <script>
                                                    new Mapkick.Map("map", [{latitude: {{ $station->latitude }}, longitude: {{ $station->longitude }}}]);
                                                  </script>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        @endforeach
                    </div> 
                </div>
            </div>
        </div>
    </section>
@endsection
