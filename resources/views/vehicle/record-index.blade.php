@extends('layouts.app')

@section('content')
    <script src="{{ mix('js/vehicle/records.js') }}" defer></script>
    <section class="content vehicle-content">
        <div class="row">
            <div class="col-lg-2 offset-lg-1 col-md-3 offset-md-1 col-sm-12">
                <div class="card card-stats mob-m-10 m-t-0">
                    <div class="card-header card-header-success card-header-icon">
                        <p class="card-category">Total Liters</p>
                        <h3 class="card-title">{{ round($fuel_summaries->liters) }}</h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-12">
                <div class="card card-stats mob-m-10 content-center m-t-0">
                    <div class="card-header card-header-success card-header-icon">
                        <p class="card-category">Fuel Cost</p>
                        <h3 class="card-title">${{ round($fuel_summaries->cost) }}</h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-12">
                <div class="card card-stats mob-m-10 m-t-0">
                    <div class="card-header card-header-success card-header-icon">
                        <p class="card-category">Total KMs</p>
                        <h3 class="card-title">{{ round($fuel_summaries->kms) }}</h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-12">
                <div class="card card-stats mob-m-10 content-center m-t-0">
                    <div class="card-header card-header-success card-header-icon">
                        <p class="card-category">Total Cost</p>
                        <h3 class="card-title">${{ round($totalCost->cost) }}</h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-12">
                <div class="card card-stats mob-m-10 content-center m-t-0">
                    <div class="card-header card-header-success card-header-icon">
                        <p class="card-category">Shared to</p>
                        <h3 class="card-title">{{ $totalUsers }} users</h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-10 offset-lg-1 col-md-10 offset-md-1 col-sm-12">
                <div class="card">
                    <div class="card-header card-header-info">
                        <h4 class="card-title">Vehicle Tracker - {{ $vehicle->friendly_name }}</h4>
                        <small>{{ $vehicle->year }} {{ $vehicle->make }} - {{ $vehicle->model }} @if (Auth::user()->id != $vehicle->user_id) | Owner: {{ $vehicle->given_name }} {{ $vehicle->family_name }} @endif</small>
                    </div>
                    <div class="card-body">
                        <div class="py-8">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <strong>{{ $message }}</strong>
                                </div>
                            @endif

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    The following error(s) occured:
                                    <ul class="m-0">
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>

                        <div class="row">
                            <a href="{{ route('vehicle.record.create', $vehicle->id) }}" class="btn btn-success"><i class="fa fa-plus fa-fw"></i> Create record</a>
                            @if (Auth::user()->id == $vehicle->user_id)
                                <a href="{{ route('vehicle.share.create', $vehicle->id) }}" class="btn btn-info"><i class="fa fa-plus fa-fw"></i> Share to another user</a>
                            @endif
                            <a class="btn btn-secondary" href="{{ route('vehicle.index') }}">Return to all vehicles</a>
                        </div>

                        <div class="row">
                            <table class="table table-stripped table-hover table-thin">
                                <thead class="text-info">
                                    <tr>
                                        <th>Type</th>
                                        <th>Date</th>
                                        <th>Cost</th>
                                        <th class="d-none d-md-table-cell d-sm-none ">Liters</th>
                                        <th class="d-none d-md-table-cell d-sm-none ">KMs</th>
                                        <th class="d-none d-md-table-cell d-sm-none ">Fuel Cost</th>
                                        <th>Description</th>
                                        <th>Added By</th>
                                        <th class="d-none d-md-table-cell d-sm-none "></th>
                                    </tr>

                                </thead>

                                <tbody>

                                    @foreach ($vehicle_records as $vehicle)
                                        <tr>
                                            <td>{{ $vehicle->type }}</td>
                                            <td>{{ date('d/m/Y', strtotime($vehicle->date)) }}</td>
                                            <td>${{$vehicle->cost}}</td>
                                            <td class="d-none d-md-table-cell d-sm-none ">{{$vehicle->liters}}</td> 
                                            <td class="d-none d-md-table-cell d-sm-none ">{{$vehicle->kms}}</td>
                                            <td class="d-none d-md-table-cell d-sm-none ">{{$vehicle->fuel_cost}}</td>
                                            <td>{{$vehicle->description}}</td>
                                            <td>{{$vehicle->given_name}} {{$vehicle->family_name}}</td>

                                            <td class="d-none d-md-table-cell d-sm-none ">
                                                <a class="btn btn-secondary btn-xs" href="{{ route('vehicle.record.update', $vehicle->id) }}">Edit</a>
                                                <a class="btn btn-secondary btn-xs archive-vehicle-btn" data-id="{{$vehicle->id}}">Archive</a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    @if (count($vehicle_records) == 0)
                                        <tr>
                                            <td colspan="9" class="text-center">There are vehicle records to display.</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                            {{ $vehicle_records->links('vendor.pagination.simple-bootstrap-4') }}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <script type="text/javascript">
        var archiveVehicleURL='{{ route("vehicle.record.destroy") }}' ;
    </script>
@endsection
