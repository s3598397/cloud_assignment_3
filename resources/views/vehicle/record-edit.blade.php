@extends('layouts.app')

@section('content')
    <script src="{{ mix('js/vehicle/record.js') }}" defer></script>
    <section class="content vehicle-content">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 col-md-10 offset-md-1 col-sm-12">
                <div class="card">
                    <div class="card-header card-header-info">
                        <h4 class="card-title">@if (isset($vehicle_record)) {{ __('Update Vehicle Record') }} @else {{ __('Create Vehicle Record') }} @endif</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" name="vehicle" action=@if (isset($vehicle_record)) {{ route('vehicle.record.update', $vehicle_record->id) }} @else {{ route('vehicle.record.store', $vehicle->id) }} @endif>
                            @csrf
                            <div class="">
                                @if ($message = Session::get('success'))
                                    <div class="alert alert-success">
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @endif

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        The following error(s) occured:
                                        <ul class="m-0">
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>

                            <div class="">
                                <div class="row">
                                    <label class="col-sm-2 col-form-label" for="input-current-password">Type <span class="col-red">*</span></label>
                                    <div class="col-sm-2">
                                        <div class="form-group bmd-form-group">
                                            <select class="form-control category-from" name="vehicle_record_type_id" required>
                                                @foreach ($vehicle_record_types as $vehicle_record_type)
                                                    <option @if (isset($vehicle_record)) {{ $vehicle_record->vehicle_record_type_id == $vehicle_record_type->id ? 'selected' : '' }} @endif value="{{ $vehicle_record_type->id }}">{{ $vehicle_record_type->type }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <label class="col-sm-2 col-form-label" for="input-current-password">Date Received <span class="col-red">*</span></label>
                                    <div class="col-sm-2">
                                        <div class="form-group bmd-form-group">
                                            <input class="form-control datepicker" type="text" name="date" placeholder="Enter Date" value="@if (isset($vehicle_record)) {{ date('d/m/Y', strtotime($vehicle_record->date)) }} @else {{ old('date') }} @endif" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label" for="input-current-password">Description</label>
                                    <div class="col-sm-6">
                                        <div class="form-group bmd-form-group">
                                            <input class="form-control" type="text" name="description" placeholder="Enter Description" value="@if (isset($vehicle_record)) {{ $vehicle_record->description }} @else {{ old('description') }} @endif" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label" for="cost">Cost <span class="col-red">*</span></label>
                                    <div class="col-sm-2">
                                        <div class="form-group bmd-form-group">
                                            <input class="form-control" name="cost" placeholder="Enter Cost" value="@if(isset($vehicle_record)){{($vehicle_record->cost)}}@else{{old('cost')}}@endif" required>
                                        </div>
                                    </div>

                                    <label class="col-sm-2 col-form-label d-none" for="liters">Liters</label>
                                    <div class="col-sm-2">
                                        <div class="form-group bmd-form-group">
                                            <input class="form-control d-none" name="liters" placeholder="Enter Liters" value="@if (isset($vehicle_record)) {{ ($vehicle_record->liters) }} @else {{ old('liters') }} @endif">
                                        </div>
                                    </div>

                                    <label class="col-sm-2 col-form-label d-none" for="kms">KMs</label>
                                    <div class="col-sm-2">
                                        <div class="form-group bmd-form-group">
                                            <input class="form-control d-none" name="kms" placeholder="Enter KMs" value="@if (isset($vehicle_record)) {{ $vehicle_record->kms }} @else {{ old('kms') }} @endif">
                                        </div>
                                    </div>

                                    <label class="col-sm-2 col-form-label d-none" for="fuel_cost">Fuel Cost</label>
                                    <div class="col-sm-2">
                                        <div class="form-group bmd-form-group">
                                            <input class="form-control d-none" name="fuel_cost" placeholder="Enter Fuel Cost" value="@if (isset($vehicle_record)) {{ $vehicle_record->fuel_cost }} @else {{ old('fuel_cost') }} @endif">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <button type="submit" class="btn btn-success float-right">@if (isset($vehicle_record)) {{ __('Update Vehicle Record') }} @else {{ __('Create Vehicle Record') }} @endif</button>
                            <a class="btn btn-secondary float-right" href="{{ route('vehicle.record.index', $vehicle->id) }}">Return to {{ $vehicle->friendly_name }}</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
