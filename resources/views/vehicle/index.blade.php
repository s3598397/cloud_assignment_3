@extends('layouts.app')

@section('content')
    <section class="content vehicle-content">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 col-md-10 offset-md-1 col-sm-12">
                <div class="card">
                    <div class="card-header card-header-info">
                        <h4 class="card-title m-b-0">
                            Vehicle's
                            <a href="{{ route('vehicle.create') }}" class="btn btn-success float-right">Add Vehicle</a>
                            @if (isset($searched) && $searched == true) <a href="{{ route('vehicle.index') }}" class="btn btn-info float-right">Return to All Vehicles</a> @endif
                        </h4>
                    </div>
                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                The following error(s) occured:
                                <ul class="m-0">
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(count($vehicles) == 0 && ((isset($searched) && $searched == false) || !isset($searched)) ) 
                            <div class="row">
                                <div class="alert alert-warning col-sm-12">
                                    <strong>You do not have any vehicles.</strong>
                                </div>
                            </div>
                        @else
                            <form method="POST" name="vehicle" action={{route('search')}}>
                                @csrf
                                <div class="row">
                                    <label class="col-sm-2 col-form-label" for="input-current-password">Search</label>
                                    <div class="col-sm-6">
                                        <div class="form-group bmd-form-group">
                                            <input class="form-control" type="text" name="search" placeholder="Enter Vehcile Name" value="{{ old('search') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <button type="submit" class="btn btn-success">Search Vehicles</button>
                                    </div>
                                    
                                </div>
                            </form>

                            @if(count($vehicles) == 0 && isset($searched) && $searched == true) 
                                <div class="row">
                                    <div class="alert alert-warning col-sm-12">
                                        <strong>No search results found. Please try again.</strong>
                                    </div>
                                </div>
                            @else
                                <div class="row">
                                    @foreach ($vehicles as $vehicle)
                                    
                                        <div class="col-md-6">
                                            <a href="{{ route('vehicle.record.index', $vehicle->id) }}">
                                                <div class="image-container">
                                                    <img src="{{$vehicle->imageURL}}">
                                                    <div class="content">
                                                        <h3>{{ $vehicle->friendly_name }}</h3>
                                                        <p class="m-b-0">{{ $vehicle->make }} - {{ $vehicle->model }}.
                                                            @if (Auth::user()->id != $vehicle->user_id)
                                                                <br>Owner: {{ $vehicle->given_name }} {{ $vehicle->family_name }}
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                @endforeach
                            @endif
                        @endif
                    </div>
                        

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
