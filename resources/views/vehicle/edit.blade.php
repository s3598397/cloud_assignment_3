@extends('layouts.app')

@section('content')
    <script src="{{ mix('js/vehicle/edit.js') }}" defer></script>
    <section class="content vehicle-content">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 col-md-10 offset-md-1 col-sm-12">
                <div class="card">
                    <div class="card-header card-header-info">
                        <h4 class="card-title">@if (isset($vehicle)) {{ __('Update Vehicle') }} @else {{ __('Create Vehicle') }} @endif</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" enctype="multipart/form-data" name="vehicle" action=@if (isset($vehicle)) {{ route('vehicle.update', $vehicle->id) }} @else {{ route('vehicle.store') }} @endif>
                            @csrf
                            <div class="">
                                @if ($message = Session::get('success'))
                                    <div class="alert alert-success">
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @endif

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        The following error(s) occured:
                                        <ul class="m-0">
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>

                            <ul class="steps" id="progressbar">
                                <li class="step step-success">
                                  <div class="step-content">
                                    <span class="step-circle">1</span>
                                    <span class="step-text">Step 1</span>
                                  </div>
                                </li>
                                <li class="step">
                                  <div class="step-content">
                                    <span class="step-circle">2</span>
                                    <span class="step-text">Step 2</span>
                                  </div>
                                </li>
                                <li class="step">
                                  <div class="step-content">
                                    <span class="step-circle">3</span>
                                    <span class="step-text">Step 3</span>
                                  </div>
                                </li>
                              </ul>
                              <fieldset>
                                <div class="row">
                                    <label class="col-sm-2 offset-sm-2 col-form-label">Vehicle Image</label>
                                    <div class="col-sm-6">
                                        <div class="form-group bmd-form-group file-upload-wrapper" data-text="Select your file!">
                                            <input 
                                                type="file"
                                                class="form-control file-upload-field" 
                                                name="image" 
                                                accept="image/png, image/jpeg"
                                                value="{{ old("image") }}">
                                            <p class="upload">Drag your files here or click in this area.</p>
                                        </div>
                                    </div>
                                </div>
                                <input type="button" name="next" class="btn btn-info next action-button" value="Next Step" />
                            </fieldset>

                            <fieldset>
                                <div class="form-card">
                                    <h2 class="fs-title text-center">Vehicle Information</h2>
                                    <div class="row">
                                        <label class="col-sm-2 offset-sm-2 col-form-label">Make</label>
                                        <div class="col-sm-6">
                                            <div class="form-group bmd-form-group">
                                                <select class="form-control" name="make">
                                                    <optgroup label="Cars">
                                                        @foreach ($cars as $car)
                                                            <option 
                                                                value="{{ $car['make_name'] }}" 
                                                                data-id="{{ $car['make_id'] }}" 
                                                                {{ (old("make") == $car['make_name'] ? "selected": (isset($vehicle->make) && $vehicle->make == $car['make_name'] ? "selected":"")) }}
                                                            >
                                                                {{ $car['make_name'] }}
                                                            </option>
                                                        @endforeach
                                                    </optgroup>
                                                    <optgroup label="Bikes">
                                                        @foreach ($bikes as $bike)
                                                            <option 
                                                                value="{{ $bike['make_name'] }}" 
                                                                data-id="{{ $bike['make_id'] }}" 
                                                                {{ (old("make") == $bike['make_name'] ? "selected": (isset($vehicle->make) && $vehicle->make == $bike['make_name'] ? "selected":"")) }}
                                                            >
                                                                {{ $bike['make_name'] }}
                                                            </option>
                                                        @endforeach
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
    
                                    <div class="row">
                                        <label class="col-sm-2 offset-sm-2 col-form-label">Year</label>
                                        <div class="col-sm-6">
                                            <div class="form-group bmd-form-group">
                                                <select class="form-control" name="year">
                                                    @foreach ($years as $year)
                                                        <option value="{{ $year }}" {{ (old("year") == $year ? "selected": (isset($vehicle->year) && $vehicle->year == $year ? "selected":"")) }}>{{ $year }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
    
                                    <div class="row">
                                        <label class="col-sm-2 offset-sm-2 col-form-label">Model</label>
                                        <div class="col-sm-6">
                                            <div class="form-group bmd-form-group">
                                                <select class="form-control" name="model">
    
                                                </select>
                                            </div>
                                        </div>
                                    </div>
    
                                    <hr>
    
                                    <div class="row">
                                        <label class="col-sm-2 offset-sm-2 col-form-label">Friendly Name</label>
                                        <div class="col-sm-6">
                                            <div class="form-group bmd-form-group">
                                                <input 
                                                    class="form-control" 
                                                    name="friendly_name" 
                                                    value="{{ isset($vehicle) ? $vehicle->friendly_name : old("friendly_name") }}">
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <input type="button" name="previous" class="btn btn-info previous action-button-previous" value="Previous" />
                                <input type="button" name="next" class="btn btn-info next action-button" value="Next Step" />
                            </fieldset>
                            <fieldset>
                                <div class="form-card">
                                    <h2 class="fs-title text-center">Success !</h2> <br><br>
                
                                </div>
                                <input type="button" name="previous" class="btn btn-info previous action-button-previous" value="Previous" />
                                <button type="submit" class="btn btn-success pull-right">@if (isset($vehicle)) {{ __('Update Vehicle') }} @else {{ __('Create Vehicle') }} @endif</button>
                            </fieldset>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>

        function getModels() {
            form = document.querySelector('form[name="vehicle"]');
            makeElem = form.querySelector('select[name="make"]');
            make = makeElem.options[makeElem.selectedIndex].getAttribute("data-id");
            year = form.querySelector('select[name="year"]').value;
    
            $.ajax({
                url: "{{ route('vehicle.models') }}",
                type: 'GET',
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    make: make,
                    year: year
                },
                dataType: 'JSON',
                success: function (data) {
                    if (data.success == true) {
                        var models = form.querySelector('select[name="model"]');
                        models.innerHTML = "";
                        for(model of data.models) {
                            option = document.createElement('option');
                            option.value = model['Model_Name'];
                            option.setAttribute('data-id', model['Model_ID']);
                            option.innerHTML = model['Model_Name'];
                            models.appendChild(option);
                        }
                    } else {
                        // alert("An error occured");
                    }
                }
            }); 
        }
        
        function validateImage() {
            form = document.querySelector('form[name="vehicle"]');
            image = $(form.querySelector('input[name="image"]'))[0].files;
    
            let formData = new FormData();
            formData.append('image', image[0]);
            formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
    
            $.ajax({
                url: "{{ route('vehicle.image') }}",
                type: 'POST',
                data: formData,
                dataType: 'JSON',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.success == true) {
                        var friendlyName = form.querySelector('input[name="friendly_name"]');
                        friendlyName.value = data.label;
                    }
                }
            }); 
        }
    
    </script>
    
@endsection

