<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\FuelStationsContoller;
use App\Http\Controllers\VehicleImageController;
use App\Http\Controllers\VehicleShareController;
use App\Http\Controllers\VehicleRecordController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(); //['verify' => true]

Route::get('/verification-code/{username}', [App\Http\Controllers\Auth\VerificationCodeController::class, 'index'])->name('account.verification');
Route::post('/verify-code/{username}', [App\Http\Controllers\Auth\VerificationCodeController::class, 'verify'])->name('verification.code');
Route::post('/veridy-resend/{username}', [App\Http\Controllers\Auth\VerificationCodeController::class, 'resend'])->name('verification.resend');

Route::middleware(['auth'])->group(function () {
    Route::get('/', function () {
        return redirect()->route('vehicle.index');
    });
    Route::get('/home', function () {
        return redirect()->route('vehicle.index');
    });

    /*
    Vehicle
    */
    Route::get('/vehicle', [VehicleController::class, 'index'])->name('vehicle.index');
    Route::get('/vehicle/create/', [VehicleController::class, 'create'])->name('vehicle.create');
    Route::post('/vehicle/create/', [VehicleController::class, 'store'])->name('vehicle.store');
    Route::post('/vehicle/archive/', [VehicleController::class, 'destroy'])->name('vehicle.destroy');
    Route::get('/vehicle/edit/{vehicle_id}/', [VehicleController::class, 'edit'])->name('vehicle.edit');
    Route::post('/vehicle/update/{vehicle_id}/', [VehicleController::class, 'update'])->name('vehicle.update');

    Route::get('/vehicle/share/{vehicle_id}/', [VehicleShareController::class, 'create'])->name('vehicle.share.create');
    Route::post('/vehicle/share/store/', [VehicleShareController::class, 'store'])->name('vehicle.share.store');
    Route::get('/vehicle/share/accept/{token}', [VehicleShareController::class, 'accept'])->name('vehicle.share.accept');


    Route::get('/vehicle/getModels', [VehicleController::class, 'getModels'])->name('vehicle.models');
    Route::post('/vehicle/image', [VehicleImageController::class, 'check'])->name('vehicle.image');
    Route::get('/vehicle/image/{vehicleId}', [VehicleImageController::class, 'retrieve'])->name('vehicle.get.image');




    Route::get('/vehicle/{vehicle_id}', [VehicleRecordController::class, 'index'])->name('vehicle.record.index');
    Route::get('/vehicle/record/create/{vehicle_id}', [VehicleRecordController::class, 'create'])->name('vehicle.record.create');
    Route::post('/vehicle/record/create/{vehicle_id}', [VehicleRecordController::class, 'store'])->name('vehicle.record.store');
    Route::post('/vehicle/record/archive/', [VehicleRecordController::class, 'destroy'])->name('vehicle.record.destroy');
    Route::get('/vehicle/record/{record_id}/', [VehicleRecordController::class, 'edit'])->name('vehicle.record.edit');
    Route::post('/vehicle/record/{record_id}/', [VehicleRecordController::class, 'update'])->name('vehicle.record.update');

    Route::post('/search', [SearchController::class, 'search'])->name('search');

    Route::get('/test', [TestController::class, 'getsTheBasePathMapping'])->name('test');


    Route::get('/stations/search', [FuelStationsContoller::class, 'index'])->name('stations.search');
    Route::post('/stations/results', [FuelStationsContoller::class, 'search'])->name('stations.results');
    
    
    
});



