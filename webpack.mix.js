const mix = require('laravel-mix');

// 1. Version all compiled assets.
mix.version();

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/creative.scss', 'public/css')
    .js('resources/js/global.js', 'public/js')
    .js('resources/js/vehicle/edit.js', 'public/js/vehicle')
    .js('resources/js/vehicle/record.js', 'public/js/vehicle')
    .js('resources/js/vehicle/records.js', 'public/js/vehicle')
    .sourceMaps();

mix.combine([
    'node_modules/dropify/dist/js/dropify.min.js',
    'node_modules/select2/dist/js/select2.min.js',
    'node_modules/sweetalert/dist/sweetalert.min.js',
    'resources/js/mapkick.js'
], 'public/js/resources.js');

mix.combine([
    'node_modules/dropify/dist/css/dropify.min.css',
    'node_modules/select2/dist/css/select2.min.css',
], 'public/css/resources.css');
